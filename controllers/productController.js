import expressAsyncHandler from "express-async-handler";
import axios from "axios";

// Models
import Product from "../models/productModel.js";
import User from "../models/userModel.js";

// ROUTE: GET api/products
// DESCRIPTION: Get Products
// Public Route
const getProducts = expressAsyncHandler(async (req, res) => {
  const products = await Product.find({ fmtitle: { '$regex': req.query.search, $options: 'i' }}).sort({ date: -1 });
  res.json(products);
});

// ROUTE: GET api/products
// DESCRIPTION: Search Products
// Public Route
const getSearchedProducts = expressAsyncHandler(async (req, res) => {
  const products = await Product.find({ fmtitle: { '$regex': req.query.search, $options: 'i' }}).sort({ date: -1 });
  res.json(products);
});

// ROUTE: GET api/products/:catid
// DESCRIPTION: Get Products by Category ID
// Public Route
const getProductsInCategory = expressAsyncHandler(async (req, res) => {
  const products = await Product.find({
    product: { $elemMatch: { id: req.params.catid } },
  }).sort({
    date: -1,
  });

  if (products.length > 0) {
    res.json(products);
  } else {
    res.status(404);
    throw new Error("There is no products with this Product ID");
  }
});

// ROUTE: POST api/products/ser
// DESCRIPTION: Get Products by Category ID
// Public Route
const getProductsInSer = expressAsyncHandler(async (req, res) => {

  var theshoulds = [];
  var thefalse = [];

  if (req.body.id3 != 99999){
  thefalse.push(
    { "eol": false }
  );
}
if (req.body.id3 === 99999){
  thefalse.push(
    { "eol": true }
  );
  theshoulds.push(
    { "eol" : true }
  );
}

  if (!!req.body.id1) {
    theshoulds.push(
      { "category.id": req.body.id1 },
      { "secondCategory.id": req.body.id2 }
    );
  }

  if (!!req.body.id2) {
    theshoulds.push(
      { "category.id": req.body.id2 },
      { "secondCategory.id": req.body.id2 }
    );
  }

  if (!!req.body.id3 && req.body.id3 != 1382 && req.body.id3 != 1490 && req.body.id3 != 99999) {
    theshoulds.push({ "formfactor.folderId": req.body.id3 });
  }
  if (req.body.id3 === 1382){
    theshoulds.push(
      { "category.id": 17 },
      { "category.id": 18 },
      { "category.id": 20 },
      { "category.id": 70 },
      { "category.id": 71 },
      { "category.id": 100142022 },
      { "category.id": 1003042022 },
      { "category.id": 1004042022 }
    );
  }
  if (req.body.id3 === 1490){
    theshoulds.push(
      { "category.id": 46 },
      { "category.id": 47 },
      { "category.id": 52 },
      { "category.id": 1005042022 },
      { "category.id": 1006042022 }
    );
  }

  const categorizedProducts = await Product.aggregate()
    .match({
      $or: theshoulds,
      $and: thefalse,
    })
    .project({
      _id: 1,
      fmtitle: 1,
      fmlink: 1,
      id: 1,
      category: 1,
      secondCategory: 1,
      description: 1,
      manufacturer: 1,
      sizeDiagonal: 1,
      resolutionMax: 1,
      technology: 1,
      brightness: 1,
      perspective: 1,
      interface: 1,
      perspective: 1,
      temperatureRange: 1,
      powerSupply: 1,
      controller: 1,
      characteristics: 1,
      diagonale: 1,
      options: 1,
      touch: 1,
      input: 1,
      output: 1,
      cpu: 1,
      functionality: 1,
      cpuModel: 1,
      chipset: 1,
      memory: 1,
      eol: 1,
    })
    .exec();

  const groups = await Product.aggregate()
    .match({
      $or: theshoulds,
      $and: thefalse,
    })
    .facet({
      manufacturer: [
        { $match: { manufacturer: { $exists: 1 } } },
        { $unwind: "$manufacturer.title" },
        { $sortByCount: "$manufacturer.title" },
      ],
      sizeDiagonal: [
        { $match: { sizeDiagonal: { $exists: 1 } } },
        { $unwind: "$sizeDiagonal.title" },
        { $sortByCount: "$sizeDiagonal.title" },
      ],
      resolutionMax: [
        { $match: { resolutionMax: { $exists: 1 } } },
        { $unwind: "$resolutionMax.title" },
        { $sortByCount: "$resolutionMax.title" },
      ],
      brightness: [
        { $match: { brightness: { $exists: 1 } } },
        { $unwind: "$brightness.title" },
        { $sortByCount: "$brightness.title" },
      ],
      interface: [
        { $match: { interface: { $exists: 1 } } },
        { $unwind: "$interface" },
        { $sortByCount: "$interface" },
      ],
      perspective: [
        { $match: { perspective: { $exists: 1 } } },
        { $unwind: "$perspective.title" },
        { $sortByCount: "$perspective.title" },
      ],
      temperatureRange: [
        { $match: { temperatureRange: { $exists: 1 } } },
        { $unwind: "$temperatureRange" },
        { $sortByCount: "$temperatureRange" },
      ],
      powerSupply: [
        { $match: { powerSupply: { $exists: 1 } } },
        { $unwind: "$powerSupply" },
        { $sortByCount: "$powerSupply" },
      ],
      controller: [
        { $match: { controller: { $exists: 1 } } },
        { $unwind: "$controller" },
        { $sortByCount: "$controller" },
      ],
      characteristics: [
        { $match: { characteristics: { $exists: 1 } } },
        { $unwind: "$characteristics" },
        { $sortByCount: "$characteristics" },
      ],
      diagonale: [
        { $match: { diagonale: { $exists: 1 } } },
        { $unwind: "$diagonale.title" },
        { $sortByCount: "$diagonale.title" },
      ],
      options: [
        { $match: { options: { $exists: 1 } } },
        { $unwind: "$options" },
        { $sortByCount: "$options" },
      ],
      input: [
        { $match: { input: { $exists: 1 } } },
        { $unwind: "$input" },
        { $sortByCount: "$input" },
      ],
      output: [
        { $match: { output: { $exists: 1 } } },
        { $unwind: "$output" },
        { $sortByCount: "$output" },
      ],
      cpu: [
        { $match: { cpu: { $exists: 1 } } },
        { $unwind: "$cpu" },
        { $sortByCount: "$cpu" },
      ],
      cpuModel: [
        { $match: { cpuModel: { $exists: 1 } } },
        { $unwind: "$cpuModel" },
        { $sortByCount: "$cpuModel" },
      ],
      functionality: [
        { $match: { functionality: { $exists: 1 } } },
        { $unwind: "$functionality" },
        { $sortByCount: "$functionality" },
      ],
      touch: [{ $sortByCount: "$touch" }],
    });

  if (categorizedProducts.length > 0) {
    const products = [groups, ...categorizedProducts];
    res.json(products);
  } else {
    res.status(404);
    throw new Error("There is no products with this Category or Series");
  }
});

// ROUTE: POST api/products/filter
// DESCRIPTION: Get Products by Category ID
// Public Route
const getFilteredProds = expressAsyncHandler(async (req, res) => {
  var filters = [];
  var matchFilter = [];
  var theshoulds = [];
  var thefalse = [];

  thefalse.push(
    { "eol": false }
  );

  if (!!req.body.id1) {
    theshoulds.push(
      { "category.id": req.body.id1 },
      { "secondCategory.id": req.body.id2 }
    );
  }

  if (!!req.body.id2) {
    theshoulds.push(
      { "category.id": req.body.id2 },
      { "secondCategory.id": req.body.id2 }
    );
  }

  if (!!req.body.id3 && req.body.id3 != 1382 && req.body.id3 != 1490) {
    theshoulds.push({ "formfactor.folderId": req.body.id3 });
  }
  if (req.body.id3 === 1382){
    theshoulds.push(
      { "category.id": 17 },
      { "category.id": 18 },
      { "category.id": 20 },
      { "category.id": 70 },
      { "category.id": 71 },
      { "category.id": 100142022 },
      { "category.id": 1003042022 },
      { "category.id": 1004042022 }
    );
  }
  if (req.body.id3 === 1490){
    theshoulds.push(
      { "category.id": 46 },
      { "category.id": 47 },
      { "category.id": 52 },
      { "category.id": 1005042022 },
      { "category.id": 1006042022 }
    );
  }
  if (req.body.forManu.length > 0) {
    filters.push({
      text: {
        query: req.body.forManu,
        path: "manufacturer.title",
      },
    });
  }

  if (req.body.forDiag.length > 0) {
    filters.push({
      text: {
        query: req.body.forDiag,
        path: "sizeDiagonal.title",
      },
    });
  }

  if (req.body.forRes.length > 0) {
    filters.push({
      text: {
        query: req.body.forRes,
        path: "resolutionMax.title",
      },
    });
  }

  if (req.body.forBright.length > 0) {
    filters.push({
      text: {
        query: req.body.forBright,
        path: "brightness.title",
      },
    });
  }

  if (req.body.forInter.length > 0) {
    filters.push({
      text: {
        query: req.body.forInter,
        path: "interface.title",
      },
    });
  }

  if (req.body.forAngle.length > 0) {
    filters.push({
      text: {
        query: req.body.forAngle,
        path: "perspective.title",
      },
    });
  }

  if (req.body.forTemp.length > 0) {
    filters.push({
      text: {
        query: req.body.forTemp,
        path: "temperatureRange.title",
      },
    });
  }

  if (req.body.forFormat.length > 0) {
    matchFilter.push({
      "controller.title": {
        "$in": req.body.forFormat
      }
    });
  }

  if (req.body.forPower.length > 0) {
    matchFilter.push({
      powerSupply: {
        "$in": req.body.forPower
      }
    });
  }
  if (req.body.forCont.length > 0) {
    matchFilter.push({
      "controller.title": {
        "$in": req.body.forCont
      }
    });
  }
  if (req.body.forOpt.length > 0) {
    matchFilter.push({
      "options.title": {
        "$in": req.body.forOpt
      }
    });
  }
  if (req.body.forSize.length > 0) {
    matchFilter.push({
      "diagonale.title": {
        "$in": req.body.forSize
      }
    });
  }
  if (req.body.forInput.length > 0) {
    matchFilter.push({
      "input.title": {
        "$in": req.body.forInput
      }
    });
  }

  if (req.body.forOutput.length > 0) {
    matchFilter.push({
      "output.title": {
        "$in": req.body.forOutput
      }
    });
  }

  if (req.body.forCpu.length > 0) {
    matchFilter.push({
      "cpu.title": {
        "$in": req.body.forCpu
      }
    });
  }
  if (req.body.forCpuModel.length > 0) {
    matchFilter.push({
      "cpuModel.title": {
        "$in": req.body.forCpuModel
      }
    });
  }
  if (req.body.forFunction.length > 0) {
    matchFilter.push({
      "functionality.title": {
        "$in": req.body.forFunction
      }
    });
  }
  req.body.forTouch !== "" &&
    req.body.forTouch.forEach((el) =>
      filters.push({
        range: {
          gt: parseInt(el) - 1,
          lt: parseInt(el) + 1,
          path: "touch",
        },
      })
    );


  let query = Product.aggregate()

  if (filters.length > 0) {
    query = query.search({
      index: "defaultFiltering",
      compound: {
        filter: filters,
      },
    })
  }

  const match = {
    $or: theshoulds
  }

  if (matchFilter.length > 0) {
    match.$and = matchFilter
  }
  const theprods = await query.match(match)
  .project({
    _id: 1,
    fmtitle: 1,
    fmlink: 1,
    id: 1,
    category: 1,
    secondCategory: 1,
    manufacturer: 1,
    sizeDiagonal: 1,
    resolutionMax: 1,
    technology: 1,
    brightness: 1,
    perspective: 1,
    interface: 1,
    perspective: 1,
    temperatureRange: 1,
    powerSupply: 1,
    controller: 1,
    characteristics: 1,
    diagonale: 1,
    touch: 1,
    cpu: 1,
    cpuModel: 1,
    functionality: 1,
    options: 1,
    input: 1,
    output: 1,
    chipset: 1,
    memory: 1,
    eol : 1,
  });

  if (theprods.length > 0) {
    const finalresponse = theprods;
    // console.log(finalresponse)
    res.json(finalresponse);
  } else {
    res.status(404);
    throw new Error("There is no products with this Filters");
  }
});

// ROUTE: GET api/products/single/:fmlink
// DESCRIPTION: Get a Product by its slug
// Public Route
const getSingleProduct = expressAsyncHandler(async (req, res) => {
  const product = await Product.findOne({
    fmlink: req.params.fmlink,
  });

  if (product) {
    res.json(product);
  } else {
    res.status(404);
    throw new Error("Product not found");
  }
});

// ROUTE: POST api/products
// DESCRIPTION: Post a new Product
// Private Route
const postProduct = expressAsyncHandler(async (req, res) => {
  const user = await User.findById(req.user._id);

  if (user) {
    const product = await Product.create(req.body);

    if (product) {
      res.status(201).json({
        product,
      });
    } else {
      res.status(400);
      throw new Error("Invalid Product Data");
    }
  }
});

// ROUTE: PUT api/products
// DESCRIPTION: Edit a product
// Private Route
const editProduct = expressAsyncHandler(async (req, res) => {
  const user = await User.findById(req.user._id);

  if (user) {
    const {
      _id,
      isLive,
      fmtitle,
      subtitle,
      fmlink,
      description,
      specialText,
      seotitle,
      featuredimg,
      category,
    } = req.body;

    const product = await Product.findById(_id);

    if (product) {
      if (isLive) {
        product.isLive = true;
      } else {
        product.isLive = false;
      }
      if (fmtitle) product.fmtitle = fmtitle;
      if (subtitle) product.subtitle = subtitle;
      if (fmlink) product.fmlink = fmlink;
      if (description) product.description = description;
      if (specialText) product.specialText = specialText;
      if (seotitle) product.seotitle = seotitle;
      if (featuredimg) product.featuredimg = featuredimg;
      if (category) product.category = category;

      const updatedProduct = await product.save();

      res.status(201).json(updatedProduct);
    } else {
      res.status(404);
      throw new Error("Product does not exist");
    }
  } else {
    res.status(500);
    throw new Error("Not Authorized");
  }
});

// ROUTE: DELETE api/products
// DESCRIPTION: Remove a product
// Private Route
const removeProduct = expressAsyncHandler(async (req, res) => {
  const product = await Product.findById(req.params.id);

  if (product) {
    res.json({
      msg: `The product "${product.fmtitle}" has been removed`,
      id: req.params.id,
    });
    await product.remove();
  } else {
    res.status(404);
    throw new Error("Product Not Found");
  }
});

// ROUTE: GET api/products/previews
// DESCRIPTION: Get Products Previews
// Public Route
const getPrevsProducts = expressAsyncHandler(async (req, res) => {
  const productsprevs = await Product.find(
    {},
    "fmtitle fmlink title url isLive id images category secondCategory createdAt updatedAt"
  ).sort({ date: -1 });

  res.json(productsprevs);
});

// ROUTE: GET api/products/image/:id
// DESCRIPTION: Get Product Image
// Public Route
const getProductImage = expressAsyncHandler(async (req, res) => {
  const config = {
    headers: { Authorization: `Bearer ${process.env.DISTEC_TOKEN}` },
  };

  const { data } = await axios.get(
    `https://www.distec.de/rest/products_files/${req.params.id}`,
    config
  );

  if (data) {
    res.json(data);
  } else {
    res.status(404);
    throw new Error("Product not found");
  }
});

export {
  getProducts,
  getSearchedProducts,
  getProductsInCategory,
  getProductsInSer,
  getFilteredProds,
  getSingleProduct,
  postProduct,
  editProduct,
  removeProduct,
  getPrevsProducts,
  getProductImage,
};
